function Dictionary(words) {
  this.words = words;
}

Dictionary.prototype.findMostSimilar = function (term) {
  let minDistance = Infinity;
  let mostSimilarWord = "";
  this.words.forEach((word) => {
    let distance = levensteinDistance(term, word);
    if (distance < minDistance) {
      minDistance = distance;
      mostSimilarWord = word;
    }
  });
  return mostSimilarWord;
};

function levensteinDistance(s1, s2) {
  const matrix = [];
  const s2len = s2.length;
  const s1len = s1.length;
  for (let i = 0; i <= s2len; i++) {
    matrix[i] = [i];
  }
  for (let j = 0; j <= s1len; j++) {
    matrix[0][j] = j;
  }
  for (let i = 1; i <= s2len; i++) {
    for (let j = 1; j <= s1len; j++) {
      if (s2.charAt(i - 1) === s1.charAt(j - 1)) {
        matrix[i][j] = matrix[i - 1][j - 1];
      } else {
        matrix[i][j] = Math.min(
          matrix[i - 1][j - 1] + 1,
          matrix[i][j - 1] + 1,
          matrix[i - 1][j] + 1
        );
      }
    }
  }
  return matrix[s2len][s1len];
}
