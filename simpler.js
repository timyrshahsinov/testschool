class Interpreter {
  constructor() {
    this.vars = {};
  }
  static tokenize(program) {
    const regex =
      /\s*([-+*\/%=\(\)]|[A-Za-z_][A-Za-z0-9_]*|[0-9]*\.?[0-9]+)\s*/g;
    return program.split(regex).filter((s) => /\S/.test(s));
  }
  input(expr) {
    const tokens = Interpreter.tokenize(expr);
    let current = 0;
    const parseExpression = () => {
      let node = parseTerm();
      while (["+", "-"].includes(tokens[current])) {
        const token = tokens[current++];
        const right = parseTerm();
        switch (token) {
          case "+":
            node += right;
            break;
          case "-":
            node -= right;
            break;
        }
      }
      return node;
    };
    const parseTerm = () => {
      let node = parseFactor();
      while (["*", "/", "%"].includes(tokens[current])) {
        const token = tokens[current++];
        const right = parseFactor();
        switch (token) {
          case "*":
            node *= right;
            break;
          case "/":
            node /= right;
            break;
          case "%":
            node %= right;
            break;
        }
      }
      return node;
    };
    const parseFactor = () => {
      let token = tokens[current++];
      if (token === "(") {
        const value = parseExpression();
        if (tokens[current++] !== ")") {
          throw new SyntaxError("Expected )");
        }
        return value;
      }
      if ("0123456789".includes(token[0])) {
        return parseFloat(token);
      }
      if (token === "=") {
        throw new SyntaxError("Unexpected assignment");
      }
      if (/[a-zA-Z_]/.test(token[0])) {
        if (tokens[current] === "=") {
          current++;
          const value = parseExpression();
          this.vars[token] = value;
          return value;
        }
        if (!this.vars.hasOwnProperty(token)) {
          throw new ReferenceError(
            `Invalid identifier. No variable with name '${token}' was found.`
          );
        }
        return this.vars[token];
      }
      throw new SyntaxError("Unknown token: " + token);
    };
    return parseExpression();
  }
}
